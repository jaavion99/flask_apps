from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask import Flask,render_template,request


app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://postgres:123@localhost:5432/flask"

app.debug = True
db  = SQLAlchemy(app)


class books(db.Model):
    __tablename__ = 'books'
    booksTitle = db.Column(db.String(100), primary_key = True)
    bookText = db.Column(db.String(), nullable = False)
    likes = db.Column(db.Integer(), nullable = False, default = 0)

    def __init__(self, bookTitle, bookText, likes):
        self.booksTitle = booksTitle
        self.bookText = bookText
        self.likes = likes


@app.route('/books', method=['GET'])
def getbooks():
    allbooks = books.query.all()
    output = []
    for book in allbooks:
        currBook = {}
        currBook['bookTitle'] = book.bookTitle
        currBook['bookTest'] = book.bookTest
        currBook['likes'] = book.likes
        output.append(currBook)
    return jsonify(output)
@app.route('/books', methods=['POST'])
def pbooks():
    bookData = request.get_json()
    book = books(bookTite=bookData['bookTitle'], bookText = bookData['bookText'], likes = bookData['likes']

